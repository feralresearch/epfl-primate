///////////////////////////////////////////////////////////
// DS_SCROLLHANDLER_FR
// 3JS scroll+tween for Macacque  
//
// Author: Andrew Sempere
// DigitalScenographic.com
// 10/19/2016
///////////////////////////////////////////////////////////

var keypoints;
var timer;


// Calculate the tween based on where we are relative to the list of keypoints
function calculatePositionFor(scrollPosition, keypoints){
	
	
	var currentPosition = {cameraX:camera.position.x,cameraY:camera.position.y,cameraZ:camera.position.z, 
						   objectX:modelObject.position.x,objectY:modelObject.position.y,objectZ:modelObject.position.z,
						   objectRotX:modelObject.rotation.x,objectRotY:modelObject.rotation.y,objectRotZ:modelObject.rotation.z};
	var indexA = 0;
	var indexB = 0;
	
	// Loop over keypoints and find the two we are between
	for (var i=0; i<keypoints.length; i++){
		thisKeypoint = keypoints[i];
		
		if (scrollPosition >= thisKeypoint.scrollPosition){
			indexA = i;
			indexB = i;
		}

		if (scrollPosition < thisKeypoint.scrollPosition){
			indexB = i;
			break;
		}
	}
	
	

	var numberOfSteps = keypoints[indexB].scrollPosition-keypoints[indexA].scrollPosition;
	var keypointBegin = keypoints[indexA];
	var keypointEnd = keypoints[indexB];
	
	// Calculate tween
	var stepNumber = scrollPosition - keypointBegin.scrollPosition;
	var x_valueAtStep = ( ((keypointEnd.cameraX-keypointBegin.cameraX) / numberOfSteps) * stepNumber) + keypointBegin.cameraX;
	var y_valueAtStep = ( ((keypointEnd.cameraY-keypointBegin.cameraY) / numberOfSteps) * stepNumber) + keypointBegin.cameraY;
	var z_valueAtStep = ( ((keypointEnd.cameraZ-keypointBegin.cameraZ) / numberOfSteps) * stepNumber) + keypointBegin.cameraZ;

	var x_oValueAtStep = ( ((keypointEnd.objectX-keypointBegin.objectX) / numberOfSteps) * stepNumber) + keypointBegin.objectX;
	var y_oValueAtStep = ( ((keypointEnd.objectY-keypointBegin.objectY) / numberOfSteps) * stepNumber) + keypointBegin.objectY;
	var z_oValueAtStep = ( ((keypointEnd.objectZ-keypointBegin.objectZ) / numberOfSteps) * stepNumber) + keypointBegin.objectZ;
	
	var x_orValueAtStep = ( ((keypointEnd.objectRotX-keypointBegin.objectRotX) / numberOfSteps) * stepNumber) + keypointBegin.objectRotX;
	var y_orValueAtStep = ( ((keypointEnd.objectRotY-keypointBegin.objectRotY) / numberOfSteps) * stepNumber) + keypointBegin.objectRotY;
	var z_orValueAtStep = ( ((keypointEnd.objectRotZ-keypointBegin.objectRotZ) / numberOfSteps) * stepNumber) + keypointBegin.objectRotZ;

	// If we've rolled off the end, just keep current
	if((indexA+1) == keypoints.length){
		return currentPosition;
	}else{
		return {cameraX:x_valueAtStep, cameraY:y_valueAtStep, cameraZ:z_valueAtStep,
		        objectX:x_oValueAtStep, objectY:y_oValueAtStep, objectZ:z_oValueAtStep,
		        objectRotX:x_orValueAtStep, objectRotY:y_orValueAtStep, objectRotZ:z_orValueAtStep};
		//console.log(scrollPosition +" is between " + indexA + " and " + indexB + " MAX: "+keypoints.length);
	}
}


function updatePositionTo(updatePositionTo){
	camera.position.x = updatePositionTo.cameraX;
	camera.position.y = updatePositionTo.cameraY;
	camera.position.z = updatePositionTo.cameraZ;
	if (typeof modelObject !== 'undefined') {
		modelObject.position.x = updatePositionTo.objectX;
		modelObject.position.y = updatePositionTo.objectY;
		modelObject.position.z = updatePositionTo.objectZ;

		modelObject.rotation.x = updatePositionTo.objectRotX;
		modelObject.rotation.y = updatePositionTo.objectRotY;
		modelObject.rotation.z = updatePositionTo.objectRotZ;
	}
}


// Document Ready
var isFading = false;
$(document).ready(function(){
	
	// Hide the content (we'll fade it in later)
	$("#content").hide();
	
	var previousScrollPosition = 0;

	// Curtain up when we're ready
	timer = window.setTimeout(curtainUp, 500);
	
	// On Scroll
	$('body').bind('touchmove', function(e) { 
			$("#downArrow").fadeOut(250);
			isFading=true;
	});

    $("#content").scroll(function(){
    	if (typeof modelObject == 'undefined') {
    		return;
		}
		var scrollPos = $("#content").scrollTop();

		if ((scrollPos > 0) && !isFading){
			$("#downArrow").fadeOut(250);
			isFading=true;
		}

	    //console.log(scrollPos);
		scrollPos = scrollPos<0?0:scrollPos;
		updatePositionTo(calculatePositionFor(scrollPos, keypoints));
		previousScrollPosition=scrollPos;
	});
});

var scrollHandler_failCount = 0;
function curtainUp(){
	if (typeof modelObject == 'undefined') {
		$("#downArrow").hide();
		window.clearTimeout(timer);
		timer=window.setTimeout(curtainUp, 500);
		console.log("NOT READY (model not loaded)..."+scrollHandler_failCount);
		scrollHandler_failCount++;
		if(scrollHandler_failCount>20){
			window.clearTimeout(timer);
			console.log("GIVING UP!");
			$("body").removeClass("loading");
			$("body").addClass("error");			
		}
	}else{
		console.log("OK: Showtime...");
		updatePositionTo(calculatePositionFor(0, keypoints));	
		$("body").removeClass("loading");
		$("body").removeClass("error");

		$("#content").fadeIn(1000, 
				function(){
					$("#downArrow").delay(2000).fadeIn(2000, 
							function(){
								//$("#downArrow").fadeOut(2000);
					}
				);
			}
		);
		

	}
}

